var colors = [
    '#38e8eb',
    '#0ec9f3',
    '#0698d1',
    '#9b7cde',
    '#d485af',
    '#fdce4c'
];

var defaultOptions = {
    xAxis: {
        lineWidth: 2,
        lineColor: '#e9f3f5',
        tickWidth: 0,
        labels: {
            style: {
                color: 'rgba(43, 58, 82, 0.54)'
            }
        }
    },
    yAxis: {
        labels: {
            style: {
                color: 'rgba(43, 58, 82, 0.38)'
            }
        }
    },
    tooltip: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: 'rgba(54, 72, 102, 0.05)',
        shadow: false,
        useHTML: true,
        formatter: function () {
            return '<div class="tooltip"><div class="title">Legend note: ' + this.x + '</div><div class="value">' + this.y + '</div></div>';
        }
    }
};

// add arrow to tooltip

(function (Highcharts) {
    Highcharts.Renderer.prototype.symbols.callout = function (x, y, w, h, options) {
        var arrowLength = 6,
            halfDistance = 6,
            r = Math.min((options && options.r) || 0, w, h),
            safeDistance = r + halfDistance,
            anchorX = options && options.anchorX,
            anchorY = options && options.anchorY,
            path;

        path = [
            'M', x + r, y,
            'L', x + w - r, y, // top side
            'C', x + w, y, x + w, y, x + w, y + r, // top-right corner
            'L', x + w, y + h - r, // right side
            'C', x + w, y + h, x + w, y + h, x + w - r, y + h, // bottom-right corner
            'L', x + r, y + h, // bottom side
            'C', x, y + h, x, y + h, x, y + h - r, // bottom-left corner
            'L', x, y + r, // left side
            'C', x, y, x, y, x + r, y // top-right corner
        ];

        path.splice(23, 3,
            'L', w / 2 + halfDistance, y + h,
            w / 2, y + h + arrowLength,
            w / 2 - halfDistance, y + h,
            x + r, y + h
        );

        return path;
    };
}(Highcharts));

// areaspline

$(function () {
    var areasplineTheme = {
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        yAxis: {
            title: {
                text: null
            }
        },
        colors: [{
            linearGradient: [0, 0, '100%', '100%'],
            stops: [
                [0, 'rgba(56, 232, 235, 0.1)'],
                [1, 'rgba(15, 201, 242, 0.1)']
            ]
        }],
        plotOptions: {
            series: {
                marker: {
                    enabled: false,
                    fillColor: '#0fc9f2',
                    lineWidth: 2,
                    lineColor: '#fff',
                    radius: 6
                },
                lineWidth: 3,
                lineColor: {
                    linearGradient: [0, 0, '100%', '100%'],
                    stops: [
                        [0, '#38e8eb'],
                        [1, '#0fc9f2']
                    ]
                }
            }
        }
    };

    var areasplineCategories = [
        '2007',
        '2008',
        '2009',
        '2010',
        '2011',
        '2012',
        '2013'
    ];

    var areasplineData = {
        xAxis: {
            categories: areasplineCategories,
            min: 0.5,
            max: areasplineCategories.length - 1.5
        },
        series: [{
            name: 'John',
            showInLegend: false,
            data: [3, 4, 3, 5, 4, 10, 12]
        }]
    };

    areasplineTheme = jQuery.extend(true, {}, defaultOptions, areasplineTheme);

    areasplineData = jQuery.extend(true, {}, areasplineTheme, areasplineData);

    var areaspline = new Highcharts.Chart('areaspline', areasplineData);
});

// areaspline inverse

$(function () {
    var areasplineInverseTheme = {
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        yAxis: {
            title: {
                text: null
            }
        },
        colors: [{
            linearGradient: [0, 0, '100%', '100%'],
            stops: [
                [0, 'rgba(155, 124, 222, 0.1)'],
                [0.5, 'rgba(212, 133, 175, 0.1)'],
                [1, 'rgba(245, 188, 66, 0.1)']
            ]
        }],
        plotOptions: {
            series: {
                marker: {
                    enabled: false,
                    fillColor: '#9b7cde',
                    lineWidth: 2,
                    lineColor: '#fff',
                    radius: 6
                },
                lineWidth: 3,
                lineColor: {
                    linearGradient: [0, 0, '100%', '100%'],
                    stops: [
                        [0, '#9b7cde'],
                        [0.5, '#d485af'],
                        [1, '#f5bc42']
                    ]
                }
            }
        }
    };

    var areasplineInverseCategories = [
        '2007',
        '2008',
        '2009',
        '2010',
        '2011',
        '2012',
        '2013'
    ];

    var areasplineInverseData = {
        xAxis: {
            categories: areasplineInverseCategories,
            min: 0.5,
            max: areasplineInverseCategories.length - 1.5
        },
        series: [{
            name: 'Bob',
            showInLegend: false,
            data: [10, 5, 7, 3, 7, 3, 2]
        }]
    };

    areasplineInverseTheme = jQuery.extend(true, {}, defaultOptions, areasplineInverseTheme);

    areasplineInverseData = jQuery.extend(true, {}, areasplineInverseTheme, areasplineInverseData);

    var areasplineInverse = new Highcharts.Chart('areaspline-inverse', areasplineInverseData);
});

// column

$(function () {
    var columnTheme = {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        yAxis: {
            title: {
                text: null
            },
            stackLabels: {
                enabled: false
            }
        },
        colors: colors,
        plotOptions: {
            column: {
                borderWidth: 0,
                stacking: 'normal',
                dataLabels: {
                    enabled: false
                }
            }
        }
    };

    var columnCategories = [
        '2001',
        '2004',
        '2007',
        '2010',
        '2013',
        '2016'
    ];

    var columnData = {
        xAxis: {
            categories: columnCategories
        },
        series: [
            {
                name: '2001',
                showInLegend: false,
                data: [10, 8, 4, 5, 9, 7]
            },
            {
                name: '2004',
                showInLegend: false,
                data: [10, 8, 4, 7, 4, 9]
            },
            {
                name: '2007',
                showInLegend: false,
                data: [8, 6, 3, 7, 9, 9]
            },
            {
                name: '2010',
                showInLegend: false,
                data: [10, 8, 6, 4, 9, 7]
            },
            {
                name: '2013',
                showInLegend: false,
                data: [null, null, null, null, 3, 4]
            },
            {
                name: '2016',
                showInLegend: false,
                data: [null, null, null, null, 5, 7]
            }
        ]
    };

    columnTheme = jQuery.extend(true, {}, defaultOptions, columnTheme);

    columnData = jQuery.extend(true, {}, columnTheme, columnData);

    var column = new Highcharts.Chart('column', columnData);
});

// pie

$(function () {
    var pieTheme = {
        colors: colors,
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            formatter: function () {
                return '<div class="tooltip"><div class="title">' + this.key + '</div><div class="value">' + this.y + '%</div></div>';
            }
        },
        plotOptions: {
            series: {
                states: {
                    hover: {
                        enabled: false
                    }
                }
            },
            pie: {
                borderWidth: 0,
                allowPointSelect: false,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                }
            }
        }
    };

    var pieData = {
        series: [{
            name: 'Browsers',
            data: [
                {
                    name: 'Chrome',
                    y: 45
                },
                {
                    name: 'Mozilla Firefox',
                    y: 25
                },
                {
                    name: 'Internet Explorer',
                    y: 10
                },
                {
                    name: 'Safari',
                    y: 20
                }
            ]
        }]
    };

    pieTheme = jQuery.extend(true, {}, defaultOptions, pieTheme);

    pieData = jQuery.extend(true, {}, pieTheme, pieData);

    var pie = new Highcharts.Chart('pie', pieData);
});

// donut

$(function () {
    var donutTheme = {
        colors: colors,
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            formatter: function () {
                return '<div class="tooltip"><div class="title">' + this.key + '</div><div class="value">' + this.y + '%</div></div>';
            }
        },
        plotOptions: {
            series: {
                states: {
                    hover: {
                        enabled: false
                    }
                }
            },
            pie: {
                innerSize: '60%',
                borderWidth: 0,
                allowPointSelect: false,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                }
            }
        }
    };

    var donutData = {
        series: [{
            name: 'Browsers',
            data: [
                {
                    name: 'Chrome',
                    y: 45
                },
                {
                    name: 'Mozilla Firefox',
                    y: 25
                },
                {
                    name: 'Internet Explorer',
                    y: 10
                },
                {
                    name: 'Safari',
                    y: 20
                }
            ]
        }]
    };

    donutTheme = jQuery.extend(true, {}, defaultOptions, donutTheme);

    donutData = jQuery.extend(true, {}, donutTheme, donutData);

    var donut = new Highcharts.Chart('donut', donutData);
});

// areaspline multiple

$(function () {
    var areasplineMultipleTheme = {
        chart: {
            type: 'areaspline'
        },
        title: {
            text: ''
        },
        yAxis: {
            title: {
                text: null
            }
        },
        colors: colors,
        plotOptions: {
            series: {
                fillOpacity: 0,
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    lineWidth: 2,
                    lineColor: '#fff',
                    radius: 6
                },
                lineWidth: 3
            }
        }
    };

    var areasplineMultipleCategories = [
        '2007',
        '2008',
        '2009',
        '2010',
        '2011',
        '2012',
        '2013'
    ];

    var areasplineMultipleData = {
        xAxis: {
            categories: areasplineMultipleCategories,
            min: 0.5,
            max: areasplineMultipleCategories.length - 1.5
        },
        series: [
            {
                name: 'Apple',
                showInLegend: false,
                data: [10, 5, 7, 3, 7, 3, 2]
            },
            {
                name: 'Apricot',
                showInLegend: false,
                data: [5, 8, 6, 4, 10, 7, 3]
            },
            {
                name: 'Banana',
                showInLegend: false,
                data: [12, 7, 10, 6, 5, 6, 9]
            },
            {
                name: 'Cherry',
                showInLegend: false,
                data: [8, 13, 5, 2, 6, 5, 8]
            }
        ]
    };

    areasplineMultipleTheme = jQuery.extend(true, {}, defaultOptions, areasplineMultipleTheme);

    areasplineMultipleData = jQuery.extend(true, {}, areasplineMultipleTheme, areasplineMultipleData);

    var areasplineMultiple = new Highcharts.Chart('areaspline-multiple', areasplineMultipleData);
});